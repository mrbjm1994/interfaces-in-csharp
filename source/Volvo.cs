
namespace interfaces.source
{
    //  A class of vovlo inherits from vehicles interface
    public class Volvo : vehicles
    {
        // declaring class properties
        public int speed;
        public int brake;
        public int gear;
        
        // define the speedUp method first declared in the vehicle interface
        // This tells Volvo, an object of Vehicles class how to speed up
        public void speedUp(int increasedSpeed)
        {
            speed = speed + increasedSpeed;
        }

        public void slowDown(int decreasedSpeed)
        {
            speed = speed - decreasedSpeed;
        }

        public void changeGear(int newGear)
        {
            gear = newGear;
        }

    }
}