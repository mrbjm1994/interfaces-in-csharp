using System;
using interfaces.source;

namespace interfaces.source
{
    public class States
    {
        // prints the states of each object
        public static void printStates()
        {
            Volvo volvo = new Volvo();
            Benz benz = new Benz();
            benz.speedUp(100);
            benz.changeGear(5);
            volvo.speedUp(5);
            volvo.changeGear(3);
            Console.WriteLine("Choose a car");
            Console.WriteLine("1: Benz");
            Console.WriteLine("2: Volvo");
            int userInput = int.Parse(Console.ReadLine());
            if (userInput == 1)
            {
                Console.WriteLine("Speed = " + benz.speed + " Gear = " + benz.gear);
            }
            else if (userInput == 2)
            {
                Console.WriteLine("Speed = " + volvo.speed + " Gear = " + benz.gear);
            }
            
        }
    }
}