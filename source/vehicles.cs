using System;

namespace interfaces.source
{
    // creating a vehicles interface
    public interface vehicles
    {
        //  declaring abstract methods
        void speedUp(int a);

        void slowDown(int a);

        void changeGear(int a);
    }
}