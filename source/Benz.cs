namespace interfaces.source
{
    public class Benz : vehicles
    {
        // declaring class properties
        public int speed;
        public int brake;
        public int gear;

        // define the speedUp method first declared in the vehicle interface
        // This tells Benz, an object of Vehicles class how to speed up
        public void speedUp(int increasedSpeed)
        {
            speed = speed + increasedSpeed;
        }

        public void slowDown(int decreasedSpeed)
        {
            speed = speed - decreasedSpeed;
        }

        public void changeGear(int newGear)
        {
            gear = newGear;
        }
    }
}